package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        int quitter = 0;
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            while (true) {
                String hu = readInput("Your choice (Rock/Paper/Scissors)?");
                if (!(hu.equals("rock")) && !(hu.equals("paper")) && !(hu.equals("scissors"))) {
                    System.out.println("I do not understand " + hu + ". Could you try again?");
                    continue;
                }
                String comp = rand();
                String win = checkWinner(hu, comp);
                System.out.println("Human chose " + hu + ", computer chose " + comp + ". " + win);
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                String cont;
                while (true) {
                    cont = readInput("Do you wish to continue playing? (y/n)?");
                    if (cont.equals("y") || cont.equals("n")) break;
                }
                if (cont.equals("y")) {
                    break;
                } else if (cont.equals("n")) {
                    System.out.println("Bye bye :)");
                    quitter = 1;
                    break;
                }
            }
            if (quitter == 1) break;
            roundCounter++;
        }
    }

    public String rand() {
        int r = (int)(Math.random() * 3);
        return rpsChoices.get(r);
    }

    public String checkWinner(String choiceHum, String choiceComp) {
        if (choiceHum.equals("rock") && choiceComp.equals("paper")) {
            computerScore++;
            return "Computer wins!";
        } else if (choiceHum.equals("rock") && choiceComp.equals("scissors")) {
            humanScore++;
            return "Human wins!";
        } else if (choiceHum.equals("paper") && choiceComp.equals("scissors")) {
            computerScore++;
            return "Computer wins!";
        } else if (choiceHum.equals("paper") && choiceComp.equals("rock")) {
            humanScore++;
            return "Human wins!";
        } else if (choiceHum.equals("scissors") && choiceComp.equals("rock")) {
            computerScore++;
            return "Computer wins!";
        } else if (choiceHum.equals("scissors") && choiceComp.equals("paper")) {
            humanScore++;
            return "Human wins!";
        } else if (choiceHum.equals("scissors") && choiceComp.equals("scissors")) {
            return "It's a tie!";
        } else if (choiceHum.equals("paper") && choiceComp.equals("paper")) {
            return "It's a tie!";
        } else if (choiceHum.equals("rock") && choiceComp.equals("rock")) {
            return "It's a tie!";
        }
        return null; 
    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
